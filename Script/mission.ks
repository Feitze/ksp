{
  local saveFile is "1:/runmode.ks".
  export({
    parameter missionDefinition.
    
    // Recover runmode if it exists
    local runmode is 0.
    if exists(saveFile) set runmode to import("runmode.ks").
    
    // Create sequence, events, and next function to pass to mission definition
    local sequence is list().
    local events is lex().
    local next is {
      parameter m is runmode + 1.
      if not exists(saveFile) create(saveFile).
      local h is open(saveFile).
      h:clear().
      h:write("export("+m+").").
      set runmode to m.
    }.
    
    // Populate the sequence and events using the definition
    missionDefinition(sequence, events, next).
    
    // Run the main mission loop until done
    return {
      until runmode >= sequence:length {
        sequence[runmode]().
        for event in events:values {
          event().
        }
        wait 0.
      }
    }.
  }).
}