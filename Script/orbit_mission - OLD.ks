// KSProgramming - Feitze de Vries, based on files by Cheers Kevin Games.

//assumes you start from circular parking orbit (although it probably works fine even if not).

reset().
notify("running orbit_mission").

local transfer is import("transfer").
local freeze is transfer["freeze"].
local mission is import("mission").


//mission parameters:
local longAscendingNode is 184.4 .
local target_i is 35.4 .
local argumentPeriapsis is 60 .   //eyeballed this one, not specified in contract.
local target_periapsis is 23064643. //kerbin is actually out of comm range, but whatever.
local target_apoapsis is 25553276.

local starting_inclination is ship:orbit:inclination.

local orbit_mission is mission({ parameter seq, ev, next.

	//first transfer to periapsis, then circularize, then adjust inclination, then move apoapsis.
	//(adjusting inclination requires less dV at higher altitude)

	//deploying antennas
	seq:add({  
		notify("deploying antennas, one targets kerbin, other active vessel").
		
		SET antenna TO SHIP:PARTSDUBBED("HighGainAntenna5").
		
		local n is 1.
		FOR ant IN antenna {
			if ant:GETMODULE("ModuleRTAntenna"):hasevent("ACTIVATE") {
				ant:GETMODULE("ModuleRTAntenna"):DOEVENT("ACTIVATE").
			}
			if n = 1 {
				ant:GETMODULE("ModuleRTAntenna"):SETFIELD("target", "kerbin").
			}
			else if n = 2 {
				ant:GETMODULE("ModuleRTAntenna"):SETFIELD("target", "active-vessel").
			}
			set n to n+1.
		}			
		lock throttle to 0.
		panels on.
		next().		
	}).		
	
	//adjusting inclination
	seq:add({		
		notify("adjusting inclination").
		
		//this part is supposed to find the correct location of node
		notify("finding target ascending/descending node").
		transfer["seek"](
			time:seconds+0.5*ship:orbit:period,
			freeze(0), freeze(0.5*ship:velocity:orbit:mag), freeze(0), 	{ 	
				parameter mnv. 				
				until mnv:eta>30 {
					set mnv:eta to mnv:eta + ship:orbit:period.
				}
				
				//for this manoeuvre it really should not be too far out
				until mnv:eta<2*ship:orbit:period {
					set mnv:eta to mnv:eta - ship:orbit:period. 
				}									
				return (											
					-abs(longAscendingNode-mnv:orbit:longitudeofascendingnode)
				). 			
			}
		).		
		
		local manEta is time:seconds+nextnode:eta.			
		wait 5. //debug
		remove nextnode.
		
		local close_enough is false.
		local dV_normal is 128+1.
		local dV_tangent is 0.
		
		until close_enough {
			//this part is supposed to find the correct dV in normal direction
			notify("finding dV in normal direction").
			transfer["seek"](
				freeze(manEta),
				freeze(0), dV_normal, freeze(dV_tangent), 	{ 	     //dV starts at one step bigger than stepsize of hillclimbing function to ensure positive value.
					parameter mnv. 
					until mnv:eta>30 {
						set mnv:eta to mnv:eta + ship:orbit:period.
					}
					
					return (											
						-abs(target_i-mnv:orbit:inclination)
					). 					
				}
			).

			set dV_normal to nextnode:normal.
			remove nextnode.		
			
			//this part corrects the dV in prograde direction to ensure a circular orbit
			notify("correcting dV in tangent direction").
			transfer["seek"](
				freeze(manEta),
				freeze(0), freeze(dV_normal), dV_tangent, 	{ 	
					parameter mnv. 
					until mnv:eta>30 {
						set mnv:eta to mnv:eta + ship:orbit:period.
					}						
					
					return (
						-mnv:orbit:eccentricity
					). 
				}
			).
			
			//debug
			notify("target inclination: "+target_i).
			notify("nextnode inclination: "+nextnode:orbit:inclination).	
			
			wait 1.			
			
			if abs(target_i-nextnode:orbit:inclination) < 0.01 and nextnode:orbit:eccentricity < 0.001 {
				set close_enough to true.
			}
			else {				
				set dV_tangent to nextnode:prograde.
				remove nextnode.
			}
		}		
		
		transfer["exec"](true).
		lock throttle to 0.
		
		next().				
	}).		
	
	//transfer to target periapsis
	seq:add({  
		notify("transfer to target periapsis").
		transfer["seek"](
			freeze(time:seconds + eta:periapsis),
			freeze(0), freeze(0), 0, { 				
				parameter mnv. 
				return -abs(target_periapsis-mnv:orbit:apoapsis).
			}
		).
		transfer["exec"](true).
		lock throttle to 0.
		next().		
	}).
	
	//circularizing
	seq:add({  
		notify("circularizing").
		transfer["seek"](
			freeze(time:seconds + eta:apoapsis),
			freeze(0), freeze(0), 0, { 
				parameter mnv. 
				return (
					-mnv:orbit:eccentricity
					-abs(starting_inclination-mnv:orbit:inclination)	//don't want it to reverse direction.
				).
			}
		).
		transfer["exec"](true).
		lock throttle to 0.
		next().		
	}).
	
		
	
	//transfer to target apoapsis
	seq:add({		
		notify("adjusting apoapsis").
		transfer["seek"](
			time:seconds+120,
			freeze(0), 0, 0, 	{ 	
									parameter mnv. 
									until mnv:eta>30 {
										set mnv:eta to mnv:eta + mnv:orbit:period.
									}
									return (
											-abs(target_apoapsis-mnv:orbit:apoapsis)
											-abs(argumentPeriapsis-mnv:orbit:argumentofperiapsis)
											). 	
								}).
		transfer["exec"](true).
		lock throttle to 0.
		
		notify("orbit_mission finished.").
		next().				
	}).
	
	
}).
export(orbit_mission).
