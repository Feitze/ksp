// KSProgramming - Feitze de Vries, based on files by Cheers Kevin Games.

reset().
notify("running commsat_mission").

local transfer is import("transfer").
local mission is import("mission").

local target_altitude is 2000000.
local target_i is 0.
local Sats is 3.

local freeze is transfer["freeze"].

local targetVessel is 0.
LIST TARGETS in vessels.
if ship:name = "CommSat I" {
	for vs in vessels {
		if vs:name = "CommSat III" {
			set targetVessel to vessel("CommSat IV").
		}
	}
}
else if ship:name = "CommSat II" {
	for vs in vessels {
		if vs:name = "CommSat I" {
			set targetVessel to vessel("CommSat I").
		}
	}
}
else if ship:name = "CommSat III" {
	for vs in vessels {
		if vs:name = "CommSat II" {
			set targetVessel to vessel("CommSat II").
		}
	}
}

notify("target vessel = "+ targetVessel).

local commsat_mission is mission({ parameter seq, ev, next.

	//adjusting inclination
	seq:add({
		if abs(target_i-ship:orbit:inclination)>0.01 {
			notify("adjusting inclination").
			transfer["seek"](
				time:seconds+120,
				freeze(0), 0, 0, 	{ 	
										parameter mnv. 
										until mnv:eta>30 {
											set mnv:eta to mnv:eta + ship:orbit:period.
										}
										return (
												-mnv:orbit:eccentricity
												-abs(target_i-mnv:orbit:inclination)
												). 	
									}).
			transfer["exec"](true).
			lock throttle to 0.
			panels on.
		}
		next().				
	}).
	
	//transferring to the desired orbit
	seq:add({
		notify("transfer starting").
		if targetVessel = 0 {
			transfer["seek"](
				freeze(time:seconds + eta:periapsis),
				freeze(0), freeze(0), 0, { 
					parameter mnv. 					
					return -abs(target_altitude-mnv:orbit:apoapsis).
				}
			).
			transfer["exec"](true).
		}
		else {	
			
			local ec is targetVessel:orbit:eccentricity.
			local ta is targetVessel:orbit:trueanomaly.
				
			local tanE2 is sqrt((1-ec)/(1+ec)) * tan(ta/2).
			local ea is 2 * arctan(tanE2).										//eccentric anomaly
			
			local ma is (ea*constant:degtorad - ec*sin(ea))*constant:radtodeg.	//mean anomaly
								
			local argument_target_NOW is ma +    								//position of target wrt periapsis	
				targetVessel:orbit:argumentofperiapsis +						//periapsis wrt ascending node.	
				targetVessel:orbit:longitudeofascendingnode.					//ascending node wrt solar prime vector.
			
			//notify("Current position of target wrt solar prime: "+argument_target_NOW).
			//notify("Current position of this craft wrt solar prime: "+ (ship:orbit:trueanomaly+ship:orbit:argumentofperiapsis+ship:orbit:longitudeofascendingnode)).
			transfer["seek"](
				freeze(time:seconds + 120),
				freeze(0), freeze(0), 0, { 
					parameter mnv.	
					return -abs(target_altitude-mnv:orbit:apoapsis).							
				}					
			).
			local dVmnv is nextnode:prograde.			
			remove nextnode.
			
			local n is 0.
			local close_enough is false.
			
			until close_enough {
				transfer["seek"](
					time:seconds+(0.5+n)*ship:orbit:period,
					freeze(0), freeze(0), freeze(dVmnv), { 	
						parameter mnv. 
						
						until mnv:eta>30 {
							set mnv:eta to mnv:eta + ship:orbit:period.
						}
						
						local argument_target_END is mod(argument_target_NOW + ( (mnv:eta+0.5*mnv:orbit:period)/targetVessel:orbit:period)*360 ,360).
						
						local argument_ship_END is mod(mnv:orbit:argumentofperiapsis+180+mnv:orbit:longitudeofascendingnode,360). 
						
						if abs((argument_target_END+(360/Sats))-argument_ship_END)<1 {
							set close_enough to true.
							notify("argument_target_END: "+argument_target_END).
							notify("argument_ship_END: "+argument_ship_END).
						}
						
						return -abs((argument_target_END+(360/Sats))-argument_ship_END). 	
					}
				).	
				set n to n+1.
			}
			transfer["exec"](true).
		}
		lock throttle to 0.
		panels on.
		next().
	}).
		
	//matching orbital period to target satellite or just circularizing
	seq:add({  
		notify("matching orbital period to target satellite or just circularizing").
		transfer["seek"](
			freeze(time:seconds + eta:apoapsis),
			freeze(0), freeze(0), 0, { 
				parameter mnv. 
				if targetVessel = 0 {
					return -mnv:orbit:eccentricity.
				}	
				else {
					return -abs(targetVessel:orbit:period-mnv:orbit:period).
				}		
			}
		).
		transfer["exec"](true).
		lock throttle to 0.
		panels on.
		next().		
	}).
	
	//deploying antennas
	seq:add({  
		notify("deploying antennas, one targets kerbin, other active vessel").
		
		SET antenna TO SHIP:PARTSDUBBED("HighGainAntenna5").
		
		local n is 1.
		FOR ant IN antenna {
			ant:GETMODULE("ModuleRTAntenna"):DOEVENT("ACTIVATE").
			if n = 1 {
				ant:GETMODULE("ModuleRTAntenna"):SETFIELD("target", "kerbin").
			}
			else if n = 2 {
				ant:GETMODULE("ModuleRTAntenna"):SETFIELD("target", "active-vessel").
			}
			set n to n+1.
		}			
		
		notify("commsat_mission finished.").
		next().		
	}).
}).
export(commsat_mission).
