// KSProgramming - Feitze de Vries, based on files by Cheers Kevin Games.

reset().
notify("running parking_orbit_mission").

local transfer is import("transfer").
local mission is import("mission").

local TARGET_ALTITUDE is 1.05*120000.
local target_i is 0.

local freeze is transfer["freeze"].
local terminalCheck is true.

local parking_orbit_mission is mission({ parameter seq, ev, next.

	//prelaunch
	seq:add({  
		notify("prelaunch").
		set ship:control:pilotmainthrottle to 0.
		gear off.
		lock throttle to 0.
		lock steering to heading(90, 90).
		wait 2.
		next().
	}).

	//launch; get to space and get the ship's apoapsis to the desired altitude.
	seq:add({  	
		set mode to 2. 
		if ALT:RADAR < 50 
		{ 
			set mode to 1. 
		} 
		if periapsis > 70000 
		{ 
			set mode to 4. 
		}
		
		until mode = 0 
		{ 
			if mode = 1 { //launch
				set warp to 0.
				notify("Countdown starting:").
				wait 1.
				notify("T-10 seconds").
				lock throttle to 1.
				wait 1.
				notify("T-9 seconds").
				wait 1.
				notify("T-8 seconds").
				wait 1.
				notify("T-7 seconds").
				wait 1.				
				notify("T-6 seco...").
				stage.
				wait 1.
				notify("......and here we GO, I guess").
				wait 2.
				set mode to 2.
			}
			else if mode = 2 { //vertical ascent
				lock steering to heading(90, 90).
				if (ship:altitude > 7500)
				{
					set mode to 3.
				}
			}
		
			else if mode = 3 { //gravity turn
				set targetPitch to max( 5, 75 * (1 - ALT:RADAR / 70000)). 
				lock steering to heading (90-target_i, targetPitch).

				if SHIP:APOAPSIS > TARGET_ALTITUDE
				{
					set mode to 4.
				}
			}
			
			else if mode = 4 { //coast to space
				lock steering to prograde.
				lock throttle to 0.
				set warp to 3.
				if SHIP:BODY:ATM:EXISTS = false or (ALT:RADAR > SHIP:BODY:ATM:HEIGHT)
				{
					set mode to 5.
				}
			}
			
			else if mode = 5 { //correcting apoapsis
				set warp to 0.
				lock steering to prograde.
				if SHIP:APOAPSIS < 0.95*TARGET_ALTITUDE
				{
					lock throttle to 1.
				}
				else if (SHIP:APOAPSIS >= 0.95*TARGET_ALTITUDE and SHIP:APOAPSIS < 0.99*TARGET_ALTITUDE)
				{
					lock throttle to 0.5.
				}
				else if (SHIP:APOAPSIS >= 0.99*TARGET_ALTITUDE and SHIP:APOAPSIS < TARGET_ALTITUDE)
				{
					lock throttle to 0.1.
				}
				else if (SHIP:APOAPSIS >= TARGET_ALTITUDE)
				{
					lock throttle to 0.
					set mode to 0.
				}
			}
			
			//staging part; runs throughout all modes.
			if stage:number > 0 {
				if maxthrust = 0 {
					stage.
				}
				SET numOut to 0.
				LIST ENGINES IN ListOfEngines. 
				FOR eng IN ListOfEngines {
					IF eng:FLAMEOUT {
						SET numOut TO numOut + 1.
					}
				}
				if numOut > 0 { 
					stage. 
				}
			}
			
			//thrust limiting to terminal velocity, only during launch, gravity turn and coasting
			if EXISTS(SHIP:BODY:ATM) and terminalCheck {
				if mode > 0 and mode < 5 and ALT:RADAR < SHIP:BODY:ATM:HEIGHT {
					//do stuff
					if SHIP:AIRSPEED > SHIP:TERMVELOCITY {
						notify("we should correct for terminal velocity!").
						set terminalCheck to false.
					}
				}
			}
			//writing measurements to file, altitude, temperature, pressure etc		
			//do stuff		
			
		}
		next().
	}).

	//circularizing
	seq:add({  
		notify("circularizing").
		transfer["seek"](
			freeze(time:seconds + eta:apoapsis),
			freeze(0), freeze(0), 0, { parameter mnv. return -mnv:orbit:eccentricity. }).
		transfer["exec"](true).
		lock throttle to 0.
		next().		
	}).
	
	//deploying omni-antenna
	seq:add({  
		notify("activating omni antenna and deploying solar panels").
				
		panels on.
		SET antenna TO SHIP:PARTSDUBBED("Communotron 16").
		
		FOR ant IN antenna {
			ant:GETMODULE("ModuleRTAntenna"):DOEVENT("ACTIVATE").			
		}
		next().		
	}).
	
	//adjusting inclination
	seq:add({
		if abs(target_i-ship:orbit:inclination)>0.1 {
			notify("adjusting inclination").
			transfer["seek"](
				time:seconds+120,
				freeze(0), 0, 0, 	{ 	
										parameter mnv. 
										until mnv:eta>30 {
											set mnv:eta to mnv:eta + ship:orbit:period.
										}
										return (
												-mnv:orbit:eccentricity
												-abs(target_i-mnv:orbit:inclination)
												). 	
									}).
			transfer["exec"](true).
			lock throttle to 0.
		}
		notify("parking_orbit_mission finished.").
		next().				
	}).
	
	
	
}).

export(parking_orbit_mission).
