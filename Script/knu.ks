{
	global notify is
	{
		parameter message.
		HUDTEXT("kOS: " + message, 10, 2, 20, YELLOW, true).
	}.
	
	local s is stack().
	local d is lex().
	global import is
	{
		parameter n.
		s:push(n).
		
		if not addons:rt:hasconnection(ship) {
			notify("warning: no connection to KSC").
			set warp to 9001.
			wait until addons:rt:hasconnection(ship).
			set warp to 0.
		}		
		
		if exists("1:/"+n) and exists("0:/"+n)
		{
			deletepath("1:/"+n).
		}
		if not exists("1:/"+n)
		{
			copypath("0:/"+n,"1:/").
		}
		runpath("1:/"+n).
		return d[n].
	}.
	global export is
	{
		parameter v.
		set d[s:pop()] to v.
	}.
}
