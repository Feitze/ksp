// Generalized Boot Script v1.1.0
// Kevin Gisi; modified by Feitze de Vries

// The ship will use updateScript to check for new commands from KSC.
SET SHIP:CONTROL:PILOTMAINTHROTTLE TO 0.

// Display a message
function notify {
  parameter message.
  HUDTEXT("kOS: " + message, 10, 2, 20, YELLOW, true).
}

// Detect whether a file exists on the specified volume
function has_file {
  parameter name.
  parameter vol.

  switch TO vol.
  LIST FILES IN allFiles.
  FOR file IN allFiles {
    IF file:NAME = name {
      switch TO 1.
      RETURN TRUE.
    }
  }
  switch TO 1.
  return false.
}

// Get a file from KSC
function DOWNLOAD {
	parameter name.

	IF HAS_FILE(name, 1) and HAS_FILE(name, 0) {
		DELETEPATH("1:/" + name).
	}
	if has_file(name, 0) {
		copypath("0:/" + name ,"1:/" + name).
	}
	else {
		notify("Error, unable to download " + "name").
	}
}

// Put a file on KSC
function upload {
	parameter name.

	IF HAS_FILE(name, 0) and HAS_FILE(name, 1) {
		DELETEPATH("0:/" + name).
	}
	IF HAS_FILE(name, 1) {
		COPYPATH("1:/" + name ,"0:/" + name).
	}
	else {
		notify("Error, unable to upload " + "name").
	}
}

function reset {
	list files in allFiles.
	FOR file in allFiles {
		if file:name = "knu.ks" or file:name = "boot" or file:name = "startup.ks" {
			//do nothing
		}
		else {			
			deletepath(file:name).
		}
	}
}

// THE ACTUAL BOOTUP PROCESS
wait 2.
notify("bootup").

if not has_file("knu.ks",1) {
	wait until addons:rt:hasconnection(ship).
	download("knu.ks").  //knu contains import and export function.
}
else {
	if addons:rt:hasconnection(ship) {
		download("knu.ks").					//getting a new version if we have connection
	}
}
runpath("1:/knu.ks").

SET updateScript TO SHIP:NAME + ".update.ks".

// If we have a connection, see if there are new instructions. If so, download and run them.
if addons:rt:hasconnection(ship). {
	notify("There is a connection, looking for: "+ updateScript).
	IF HAS_FILE(updateScript, 0) {
		notify("downloading updateScript").
		DOWNLOAD(updateScript).
		DELETEPATH("0:/" + updateScript). 
		IF HAS_FILE("update.ks", 1) {
			DELETEPATH("1:/update.ks").
		}
		MOVEPATH(updatescript , "update.ks").
		RUN update.ks.
		DELETEPATH("update.ks").
	} 
}

if has_file("startup.ks", 1) {
	notify("running startup.ks").
	RUN startup.ks.
} 
else {
	wait until addons:rt:hasconnection(ship).
	notify("rebooting in 10 seconds").
	wait 10. 
	reboot.
}
